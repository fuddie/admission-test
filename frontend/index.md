# Fuddie Frontend Test

##  Description
I would like to see a list of the latest github comments (https://developer.github.com/v3/search/#search-topics) based on my search data.

If I click on an element I want to see the full description.

## Requirement
* Node (required for Backend)
* JavaScript
* Pagination of 5 into 5 items and page the rest
* File README.md with instructions/explanations on how to execute and test the project

### Desirable
* Responsive layout
* ES6
* A modern MVC Framework (Express, Hapi, Sails, etc)
* React or Vue or Angular
* Redux or any other State Management library 
* Unit tests
* Infinite scroll

### Differential
* Typescript
* React Native
* Autocontained Apps (Docker)
* CI pipelines

### Evaluation criteria
* Creativity: The previous instructions do not limit any desire of the developer, be free
* Organization: project structure, versioning
* Good practices, Standards (PSR, Linteners, etc)
* Technology: use of paradigms, frameworks and libraries
* Design patterns, OOP, S.O.L.I.D. Principles,  

