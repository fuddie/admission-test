# Fuddie Backend Test

##  Description

### Cash Machine

* Create a model to represent a User with the follow uses cases
* A user has N accounts
* An account could be credit or debit type
* An account has X credit or available amount 
* An user can withdraw from credit account with 10% fee if the user has credit and is less than required 
* An user can pay for hes credit account only 
* An user can withdraw from debit account only if the user has the available amount and is less than required  
* An user can deposit more to hes debit account
#### Note: feel free to design the Model and attributes

## Requirement
* PHP >= 7.1
* Database MySQL or MariaDB
* A modern MVC Framework (Symfony, Laravel, Phalcon, etc...)
* File README.md with instructions/explanations on how to execute and test the project

### Differential
* RestFul API
* Unit tests 
* Autocontained App (Docker)
* CI pipelines

### Evaluation criteria
* Creativity: The previous instructions do not limit any desire of the developer, be free
* Organization: project structure, versioning
* Good practices, Standards (PSRs, Linteners, etc)
* Technology: use of paradigms, frameworks and libraries
* Design patterns, OOP, S.O.L.I.D. principles
